# README #

These are small libraries with implementations of the quickselect and quicksort algorithms with some minor optimizations.

The C stdlib does contain a `qsort` function but does not expose the `partition/split` function, so it is not possible to have a simple implementation of `qselect` calling library code.

### How do I get set up? ###

Simply run

```
make
```

and it will create the `libqselect.a` and `libqselect3.a` libraries that can be statically linked into your code.

If you want to run some benchmarks, run

```
make benchmark
```

then you can run the `benchmark.exe` program.

### Implementation remarks ###

In order to optimize the cost of the calls to `swap()`, you may tweak the value of `MAX_CHUNK_SIZE` in the makefile.

Text-book explanations of quicksort usually use indices only so that they can easily translate them to most programming languages. I've tweaked my implementation to use pointers in some places to reduce the number of weird conversions to arbitrary element-sized pointers, but there are still some indices. I may change it in the future to only use pointers to simplify the code.

### License ###

MIT License

Copyright (c) 2021 Eduardo Miravalls Sierra

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

### Benchmarks ###

I've tested the implementation on my Ryzen 1600 @ 3875 Mhz. Both `qselect()` and `qselect3way()` beat sorting by at least 8-10x and my custom sorts are on par.

Before using this library, keep in mind that you need to test them with your data (your mileage will vary). I don't recommend to use in production without extensive testing.

Sample outputs from `./benchmark.exe 10000001`:

```
           n:      10000001
        seed:    1640777405
     qselect:    0.096399s:    9500000    4936976 swaps   27956029 cmps
    qselect3:    0.096825s:    9500000    4915273 swaps   28429232 cmps
   qsort3way:    1.159874s:    9500000   49178172 swaps  269935759 cmps
      qsort2:    1.119488s:    9500000   48430607 swaps  267512795 cmps
       qsort:    1.290829s:    9500000          0 swaps  220103485 cmps
       msort:    1.262535s:    9500000    2096896 swaps  228136245 cmps
     speedup:   13.390481
    speedup3:   13.331567
speedup3sort:    1.112905
 speedupsort:    1.153053
speedupmsort:    1.022410
```

Sample output from `./benchmark.exe 1000000000 1640611499`

```
           n:    1000000000
        seed:    1640611499
     qselect:    4.919596s:  950000000  264537063 swaps 1469349887 cmps
    qselect3:    5.408439s:  950000000  273755708 swaps 1457191886 cmps
   qsort3way:  148.470004s:  950000000 6523872895 swaps 34318935830 cmps
      qsort2:  143.546165s:  950000000 6437173771 swaps 34431844356 cmps
       qsort:  162.445600s:  950000000          0 swaps 28642798729 cmps
       msort:  157.634471s:  950000000  231572281 swaps 29431180427 cmps
     speedup:   33.020110
    speedup3:   30.035580
speedup3sort:    1.094131
 speedupsort:    1.131661
speedupmsort:    1.030521
```

Sample outputs from `./benchmark.exe 10000001 t 0.5`:

```
           n:      10000001
        seed:    1640777411
     qselect:    0.079395s:    9500000    4133662 swaps   25120243 cmps
    qselect3:    0.083762s:    9500000    4225992 swaps   26253006 cmps
   qsort3way:    1.144781s:    9500000   51397745 swaps  257874788 cmps
      qsort2:    1.108829s:    9500000   48643086 swaps  264561946 cmps
       qsort:    1.284849s:    9500000          0 swaps  220102157 cmps
       msort:    1.242255s:    9500000    2096641 swaps  228131446 cmps
     speedup:   16.182996
    speedup3:   15.339283
speedup3sort:    1.122354
 speedupsort:    1.158744
speedupmsort:    1.034288
```

Sample outputs from `./benchmark.exe 1000000000 1640611499 0.5`:

```
           n:    1000000000
        seed:    1640611499
     qselect:    9.276829s:  950000000  477040719 swaps 2516384969 cmps
    qselect3:   11.225285s:  950000000  562117054 swaps 2777297013 cmps
   qsort3way:  146.382012s:  950000000 6737063033 swaps 33567367531 cmps
      qsort2:  142.138805s:  950000000 6464853463 swaps 33922782122 cmps
       qsort:  162.237423s:  950000000          0 swaps 28642762857 cmps
       msort:  157.481324s:  950000000  231577223 swaps 29431094969 cmps
     speedup:   17.488457
    speedup3:   14.452856
speedup3sort:    1.108315
 speedupsort:    1.141401
speedupmsort:    1.030201
```

### Sources ###

Based on

* https://www.coursera.org/learn/algorithms-part1
* https://www.cs.princeton.edu/~rs/talks/QuicksortIsOptimal.pdf
* https://en.wikipedia.org/wiki/Quicksort
* https://en.wikipedia.org/wiki/Quickselect
* https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle

with my own changes.
