/*
 * MIT License
 *
 * Copyright (c) 2021 Eduardo Miravalls Sierra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _LIB_QSELECT_H_
#define _LIB_QSELECT_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include "auxqselect.h"

/**
 * @brief      quicksort partition function.
 * Puts a pivot element in place, so that all the elements less than the pivot
 * are swapped to its left, the rest are moved to its right.
 * The pivot is selected using median3().
 *
 * @param      data       pointer to first element in the array.
 * @param      n          number of elements in data.
 * @param      lo         first element index to check.
 * @param      hi         last element index to check.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 * @param      cmpf       pointer to comparison function.
 * @param      exch       pointer to ptrswap function.
 *
 * @return     pointer to element of membsize bytes that goes at index k
 */
size_t partition(char *data, size_t n, size_t lo, size_t hi, size_t membsize, comparf cmpf, ptrswapf exch);

/**
 * @brief      find the element that goes at index k.
 *
 * @param      data       pointer to first element in the array.
 * @param      nmemb      number of elements of membsize bytes each in the array.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 * @param      cmpf       pointer to comparison function.
 * @param      k          valid element index in data.
 *
 * @return     pointer to element of membsize bytes that goes at index k
 */
void *qselect(void *data, size_t nmemb, size_t membsize, comparf cmpf, size_t k);

/**
 * @brief      sort the array using quicksort.
 * When the array contains <= CUTOFF_SIZE elements, it switches to insertsort.
 * The function name is qsort2 to avoid clashing with stdlib qsort.
 *
 * @param      data       pointer to first element in the array.
 * @param      nmemb      number of elements of membsize bytes each in the array.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 * @param      cmpf       pointer to comparison function.
 *
 * @return     nothing (data is sorted in-place).
 */
void qsort2(void *data, size_t nmemb, size_t membsize, comparf cmpf);

#ifdef  __cplusplus
}
#endif

#endif /* _LIB_QSELECT_H_ */

