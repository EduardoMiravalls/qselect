/*
 * MIT License
 *
 * Copyright (c) 2021 Eduardo Miravalls Sierra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "qselect.h"

// C. A. R. Hoare partition scheme
// returns the index of pivot
size_t partition(char *data, size_t n, size_t lo, size_t hi, size_t membsize, comparf cmpf, ptrswapf exch)
{
	size_t i = lo, // index of gte. NOTE: first for starts at i == lo + 1
	       j = hi; // index of lte. NOTE: first for starts at j == hi + 1
	// Assume lo <= mid <= hi because of median3()
	size_t pivotIdx = selectPivot(data, n, lo, hi, membsize, cmpf, exch);
	char *iptr = &data[i * membsize],
	     *jptr = &data[j * membsize],
	     *pivot = &data[pivotIdx * membsize];

	if (hi <= lo) {
		return pivotIdx;
	}

	for (;;) {
		// find "gte": first element >= pivot from the left
		// test (i < hi) not needed as median() has sorted lo <= mid <= hi
		for (iptr += membsize; cmpf(iptr, pivot) < 0; iptr += membsize) ;
		// find "lte": first element <= pivot from the right
		// test (lo < j) not needed as median() has sorted lo <= mid <= hi
		for (jptr -= membsize; cmpf(pivot, jptr) < 0; jptr -= membsize) ;
		// stop if pointers have crossed
		if (iptr >= jptr) {
			assert(jptr + membsize == iptr || jptr == iptr);
			break;
		}
		// exchange lte and gte
		exch(iptr, jptr, membsize);
		// check if pivot changed place and update pointer
		if (pivot == iptr) {
			pivot = jptr;

		} else if (pivot == jptr) {
			pivot = iptr;
		}
	}
	// put the pivot in place
	if (pivot < jptr) {
		exch(pivot, jptr, membsize);
		pivot = jptr;

	} else if (iptr < pivot) {
		exch(pivot, iptr, membsize);
		pivot = iptr;
	}

	pivotIdx = (pivot - data) / membsize;
	return pivotIdx;
}

void *_qselect(char *data, size_t lo, size_t hi, size_t membsize, comparf cmpf, ptrswapf exch, size_t k)
{
	const size_t n = hi - lo + 1;
	size_t pivotIdx;

	if (hi <= lo) {
		return &data[membsize * lo];

	} else if (n <= CUTOFF_SIZE) {
		return slowSelect(data, lo, hi, membsize, cmpf, k);
	}

	assert(lo <= k && k <= hi);
	pivotIdx = partition(data, n, lo, hi, membsize, cmpf, exch);

	if (pivotIdx < k) {
		return _qselect(data, pivotIdx + 1, hi, membsize, cmpf, exch, k);

	} else if (k < pivotIdx) {
		return _qselect(data, lo, pivotIdx - 1, membsize, cmpf, exch, k);
	}

	return &data[k * membsize];
}

void *qselect(void *data, size_t nmemb, size_t membsize, comparf cmpf, size_t k)
{
	ptrswapf exch = choosePtrswap(membsize);

	if (nmemb <= 1) {
		return data;
	}

	if (k < nmemb) {
		return _qselect(data, 0, nmemb - 1, membsize, cmpf, exch, k);

	} else {
		return _qselect(data, 0, nmemb - 1, membsize, cmpf, exch, nmemb - 1);
	}
}

void _qsort2(char *data, size_t lo, size_t hi, size_t membsize, comparf cmpf, ptrswapf exch)
{
	const size_t n = hi - lo + 1;
	size_t pivotIdx;

	if (hi <= lo) {
		return;

	} else if (n <= CUTOFF_SIZE) {
		insertSort(data, lo, hi, membsize, cmpf);
		return;
	}

	pivotIdx = partition(data, n, lo, hi, membsize, cmpf, exch);

	// recurse on the smallest array first
	if (lo == pivotIdx) {
		_qsort2(data, pivotIdx + 1, hi, membsize, cmpf, exch);

	} else if (pivotIdx <= n/2) {
		_qsort2(data, lo, pivotIdx - 1, membsize, cmpf, exch);
		_qsort2(data, pivotIdx + 1, hi, membsize, cmpf, exch);

	} else {
		_qsort2(data, pivotIdx + 1, hi, membsize, cmpf, exch);
		_qsort2(data, lo, pivotIdx - 1, membsize, cmpf, exch);
	}
}

void qsort2(void *data, size_t nmemb, size_t membsize, comparf cmpf)
{
	ptrswapf exch = choosePtrswap(membsize);

	if (nmemb <= 1) {
		return;
	}

	_qsort2(data, 0, nmemb-1, membsize, cmpf, exch);
}
