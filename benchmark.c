/*
 * MIT License
 *
 * Copyright (c) 2021 Eduardo Miravalls Sierra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string.h> // memmove

#include "qselect.h"
#include "qselect3.h"

int uniform(int min, int max)
{
	const int r = rand();
	return (max - min + 1) * (r / (1.0 * RAND_MAX));
}

void initchars(char *data, size_t n, int dups, double pDup)
{
	size_t i;

	for (i = 0; i < n; i++) {
		data[i] = (char)i;
	}

	if (dups) {
		const int resolution = 1000;
		const int p = resolution * pDup;

		for (i = 1; i < n; i++) {
			const int u = uniform(0, resolution);

			if (u <= p) {
				data[i] = data[i-1];
			}
		}
	}
}

void initshorts(short *data, size_t n, int dups, double pDup)
{
	size_t i;

	for (i = 0; i < n; i++) {
		data[i] = (short)i;
	}

	if (dups) {
		const int resolution = 1000;
		const int p = resolution * pDup;

		for (i = 1; i < n; i++) {
			const int u = uniform(0, resolution);

			if (u <= p) {
				data[i] = data[i-1];
			}
		}
	}
}

void initints(void *data, size_t n, size_t membsize, int dups, double pDup)
{
	size_t i;
	char *r = data;
	char *s;

	/* since each member has at least 4 bytes, write its value at the beggining
	 * and the end of the range so it is easy to check the values were copied
	 * a valid 12 byte number at i=0x00000004 would be: 0x000000040000000000000004 */
	for (i = 1; i < n; i++) {
		*(int *)r = (int)i;
		((int *)(r + membsize))[-1] = (int)i;
		r += membsize;
	}

	if (dups) {
		const int resolution = 1000;
		const int p = resolution * pDup;
		/* keep two pointers r points to data[i-1] and s points to data[i] */
		r = data;
		s = data + membsize;

		for (i = 1; i < n; i++) {
			const int u = uniform(0, resolution);

			if (u <= p) {
				// data[i] = data[i-1], and leave the padding (if any)
				*(int *)s = *(int *)r;
			}

			r += membsize;
			s += membsize;
		}
	}
}

void initdata(void *data, size_t n, size_t membsize, int dups, double pDup)
{
	size_t i;

	if (0 == n) {
		return;
	}

	switch (membsize) {
		case sizeof(char):
			initchars(data, n, dups, pDup);
			break;

		case sizeof(short):
			initshorts(data, n, dups, pDup);
			break;

		default:
			initints(data, n, membsize, dups, pDup);
			break;
	}

	// Knuth shuffle
	for (i = n-1; i != 0; i--) {
		const size_t u = uniform(0, i);
		swap(data, i, u, membsize);
	}
}

void *dupdata(const void *data, size_t n, size_t membsize)
{
	int *cp = calloc(n, membsize);

	if (NULL == cp) {
		return cp;
	}

	return memmove(cp, data, n * membsize);
}

int perc95(void *data, size_t n, size_t membsize, comparf cmpf)
{
	const size_t k = 95 * n / 100;
	int *res = qselect(data, n, membsize, cmpf, k);
	return *res;
}

int perc95_3(void *data, size_t n, size_t membsize, comparf cmpf)
{
	const size_t k = 95 * n / 100;
	int *res = qselect3way(data, n, membsize, cmpf, k);
	return *res;
}

void msort(void *data, size_t nmemb, size_t membsize, comparf cmpf);
typedef void (*sortf)(void *, size_t, size_t, comparf);

int perc95sort(void *data, size_t n, size_t membsize, comparf cmpf, sortf sort)
{
	const size_t k = 95 * n / 100;
	sort(data, n, membsize, cmpf);
	int *res = (int *)&((char *)data)[k * membsize];
	return *res;
}

int perc95qsort2(void *data, size_t n, size_t membsize, comparf cmpf)
{
	return perc95sort(data, n, membsize, cmpf, qsort2);
}

int perc95_3sort(void *data, size_t n, size_t membsize, comparf cmpf)
{
	return perc95sort(data, n, membsize, cmpf, qsort3way);
}

int perc95msort(void *data, size_t n, size_t membsize, comparf cmpf)
{
	return perc95sort(data, n, membsize, cmpf, msort);
}

int perc95qsort(void *data, size_t n, size_t membsize, comparf cmpf)
{
	return perc95sort(data, n, membsize, cmpf, qsort);
}

double timeval2double(struct timeval tv)
{
	return tv.tv_sec + tv.tv_usec / 1000000.0;
}

typedef int (*testfunc)(void *, size_t, size_t, comparf);

void checkSorted(const void *data, size_t n, size_t membsize, comparf cmpf, const char *name)
{
	size_t i;
	const char *p = data;
	const char *q = p + membsize;

	for (i = 1; i < n; i++) {
		if (cmpf(q, p) < 0) {
			int v, w;
			switch (membsize) {
				case sizeof(char):
					v = (unsigned int)*(char *)p;
					w = (unsigned int)*(char *)q;
					break;

				case sizeof(short):
					v = (unsigned int)*(short *)p;
					w = (unsigned int)*(short *)q;
					break;

				default:
					v = *(int *)p;
					w = *(int *)q;
					break;
			}

			printf("ERROR: %s not sorted at %zu: %d > %d\n", name, i, v, w);
			break;
		}

		p += membsize;
		q += membsize;
	}
}

void runTest(testfunc f, const void *data, size_t n, size_t membsize, comparf cmpf, int *v, struct timeval *runtime, int sorts, const char *name)
{
	struct timeval beg, end;
	char *cp = dupdata(data, n, membsize);

	resetInstrumentation();
	gettimeofday(&beg, NULL);
	*v = f(cp, n, membsize, cmpf);
	gettimeofday(&end, NULL);
	timersub(&end, &beg, runtime);

	if (sorts) {
		checkSorted(cp, n, membsize, cmpf, name);
	}

	free(cp);
}

int main(int argc, char const *argv[])
{
	struct timeval selectDiff = {}, sortDiff = {}, threeWayDiff = {}, threeWaySortDiff = {}, qsort2Diff = {}, msortDiff = {};
	size_t points = 100;
	double speedup;
	void *data;
	int v, seed = time(NULL), duplicates = 0;
	double pDup = 0; // probability of data[i] == data[i+1]
	size_t membsize = 4;
	comparf cmpf = (comparf)cmpints;
	// change n
	if (1 < argc) {
		points = strtoul(argv[1], NULL, 10);
	}
	// change seed
	if (2 < argc && 't' != argv[2][0]) {
		seed = atoi(argv[2]);
	}
	// enable duplicates in data
	if (3 < argc) {
		pDup = atof(argv[3]);
		duplicates = (0.001 <= pDup);
	}
	// change membsize
	if (4 < argc) {
		membsize = strtoul(argv[4], NULL, 0);

		switch (membsize) {
			case sizeof(char):
				cmpf = (comparf)cmpchars;
				break;

			case sizeof(short):
				cmpf = (comparf)cmpshorts;
				break;

			default:
				cmpf = (comparf)cmpints;
				break;
		}
	}

	printf("           n: %13zu\n", points);
	printf("        seed: %13d\n", seed);
	printf("        pDup: %13f\n", pDup);
	printf("    membsize: %13zu\n", membsize);
	printf("  total size: %13zuB (~%.3f MiB)\n", membsize*points, (membsize*points) / 1024.0 / 1024.0);

	srand(seed);
	data = calloc(points, membsize);

	if (NULL == data) {
		perror("calloc");
		return -1;
	}

	initdata(data, points, membsize, duplicates, pDup);

	runTest(perc95, data, points, membsize, cmpf, &v, &selectDiff, 0, "qselect");
	printf("     qselect: %4lu.%06lus: %10d %10zu swaps %10zu cmps\n",
		selectDiff.tv_sec, selectDiff.tv_usec, v, SWAPS, ICMPS);

	runTest(perc95_3, data, points, membsize, cmpf, &v, &threeWayDiff, 0, "qselect3");
	printf("    qselect3: %4lu.%06lus: %10d %10zu swaps %10zu cmps\n",
		threeWayDiff.tv_sec, threeWayDiff.tv_usec, v, SWAPS, ICMPS);

	runTest(perc95_3sort, data, points, membsize, cmpf, &v, &threeWaySortDiff, 1, "qsort3way");
	printf("   qsort3way: %4lu.%06lus: %10d %10zu swaps %10zu cmps\n",
		threeWaySortDiff.tv_sec, threeWaySortDiff.tv_usec, v, SWAPS, ICMPS);

	runTest(perc95qsort2, data, points, membsize, cmpf, &v, &qsort2Diff, 1, "qsort2");
	printf("      qsort2: %4lu.%06lus: %10d %10zu swaps %10zu cmps\n",
		qsort2Diff.tv_sec, qsort2Diff.tv_usec, v, SWAPS, ICMPS);

	runTest(perc95qsort, data, points, membsize, cmpf, &v, &sortDiff, 1, "qsort");
	printf("       qsort: %4lu.%06lus: %10d %10zu swaps %10zu cmps\n",
		sortDiff.tv_sec, sortDiff.tv_usec, v, SWAPS, ICMPS);

	runTest(perc95msort, data, points, membsize, cmpf, &v, &msortDiff, 1, "msort");
	printf("       msort: %4lu.%06lus: %10d %10zu swaps %10zu cmps\n",
		msortDiff.tv_sec, msortDiff.tv_usec, v, SWAPS, ICMPS);

	speedup = timeval2double(sortDiff) / timeval2double(selectDiff);
	printf("     speedup: %11.6f\n", speedup);
	speedup = timeval2double(sortDiff) / timeval2double(threeWayDiff);
	printf("    speedup3: %11.6f\n", speedup);
	speedup = timeval2double(sortDiff) / timeval2double(threeWaySortDiff);
	printf("speedup3sort: %11.6f\n", speedup);
	speedup = timeval2double(sortDiff) / timeval2double(qsort2Diff);
	printf(" speedupsort: %11.6f\n", speedup);
	speedup = timeval2double(sortDiff) / timeval2double(msortDiff);
	printf("speedupmsort: %11.6f\n", speedup);

	free(data);
	return 0;
}
