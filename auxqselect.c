/*
 * MIT License
 *
 * Copyright (c) 2021 Eduardo Miravalls Sierra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h> // memmove
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <inttypes.h>

#include "auxqselect.h"

size_t ICMPS = 0;
size_t SWAPS = 0;
#ifndef INSTRUMENT
	#define INSTRUMENT 0
#endif

void resetInstrumentation(void)
{
	ICMPS = 0;
	SWAPS = 0;
}

int cmpints(const int *a, const int *b)
{
#if INSTRUMENT
	ICMPS++;
#endif
	return (*b < *a) - (*a < *b);
}

int cmpshorts(const short *a, const short *b)
{
#if INSTRUMENT
	ICMPS++;
#endif
	return (*b < *a) - (*a < *b);
}

int cmpchars(const char *a, const char *b)
{
#if INSTRUMENT
	ICMPS++;
#endif
	return (*b < *a) - (*a < *b);
}

/* This function can be implemented using only a stack-based temporal item or
 * by asking the user to provide the temporal variable as qsort is not allowed to fail
 * due to allocation. In order to provide the same library call as qsort(), we use a
 * stack-allocated array instead of a global variable.
 */
void ptrswap(void *a, void *b, size_t membsize)
{
#ifndef COPY_IN_ONE_CHUNK
	#define COPY_IN_ONE_CHUNK 0
#endif
	/* tune MAX_CHUNK_SIZE to reduce number of calls to memmove/memcpy
	 * by using more stack space
	 */
	char chunk[MAX_CHUNK_SIZE];
	size_t i = MAX_CHUNK_SIZE;
	char *roverA = a, *roverB = b;
	// optimize swaps with itself
	if (a == b) {
		return;
	}

	#if !COPY_IN_ONE_CHUNK
		// copy in chunks
		if (MAX_CHUNK_SIZE <= membsize) {
			for (;i <= membsize; i += MAX_CHUNK_SIZE) {
				memmove(chunk, roverA, MAX_CHUNK_SIZE);
				memmove(roverA, roverB, MAX_CHUNK_SIZE);
				memmove(roverB, chunk, MAX_CHUNK_SIZE);
				roverA += MAX_CHUNK_SIZE;
				roverB += MAX_CHUNK_SIZE;
			}
		}
	#endif
	// copy the last chunk of size < MAX_CHUNK_SIZE
	memmove(chunk, roverA, membsize + MAX_CHUNK_SIZE - i);
	memmove(roverA, roverB, membsize + MAX_CHUNK_SIZE - i);
	memmove(roverB, chunk, membsize + MAX_CHUNK_SIZE - i);
#if INSTRUMENT
	SWAPS++;
#endif
}

void swap(void *data, size_t i, size_t j, size_t membsize)
{
	char *d = (char *)data; // void *arithmetic not allowed
	char *a = &d[i * membsize];
	char *b = &d[j * membsize];
	ptrswap(a, b, membsize);
}

#define SWAPFUNC(T)                           \
	T c;                                      \
	/* optimize swaps with itself */          \
	if (a == b) {                             \
		return;                               \
	}                                         \
	c = *(T *)a;                              \
	*(T *)a = *(T *)b;                        \
	*(T *)b = c;                              \

void ptrswap1(void *a, void *b, size_t membsize)
{
	SWAPFUNC(uint8_t);
	(void)membsize; // suppress warning when DNDEBUG is defined
	assert(sizeof(uint8_t) == membsize);
#if INSTRUMENT
	SWAPS++;
#endif
}

void swap1(void *data, size_t i, size_t j, size_t membsize)
{
	uint8_t *d = (uint8_t *)data; // void *arithmetic not allowed
	uint8_t *a = &d[i];
	uint8_t *b = &d[j];
	ptrswap1(a, b, membsize);
}

void ptrswap2(void *a, void *b, size_t membsize)
{
	SWAPFUNC(uint16_t);
	(void)membsize; // suppress warning when DNDEBUG is defined
	assert(sizeof(uint16_t) == membsize);
#if INSTRUMENT
	SWAPS++;
#endif
}

void swap2(void *data, size_t i, size_t j, size_t membsize)
{
	uint16_t *d = (uint16_t *)data; // void *arithmetic not allowed
	uint16_t *a = &d[i];
	uint16_t *b = &d[j];
	ptrswap2(a, b, membsize);
}

void ptrswap4(void *a, void *b, size_t membsize)
{
	SWAPFUNC(uint32_t);
	(void)membsize; // suppress warning when DNDEBUG is defined
	assert(sizeof(uint32_t) == membsize);
#if INSTRUMENT
	SWAPS++;
#endif
}

void swap4(void *data, size_t i, size_t j, size_t membsize)
{
	uint32_t *d = (uint32_t *)data; // void *arithmetic not allowed
	uint32_t *a = &d[i];
	uint32_t *b = &d[j];
	ptrswap4(a, b, membsize);
}

void ptrswap8(void *a, void *b, size_t membsize)
{
	SWAPFUNC(uint64_t);
	(void)membsize; // suppress warning when DNDEBUG is defined
	assert(sizeof(uint64_t) == membsize);
#if INSTRUMENT
	SWAPS++;
#endif
}

void swap8(void *data, size_t i, size_t j, size_t membsize)
{
	uint64_t *d = (uint64_t *)data; // void *arithmetic not allowed
	uint64_t *a = &d[i];
	uint64_t *b = &d[j];
	ptrswap8(a, b, membsize);
}

ptrswapf choosePtrswap(size_t membsize)
{
	switch (membsize) {
		case sizeof(uint8_t):
			return ptrswap1;
			break;

		case sizeof(uint16_t):
			return ptrswap2;
			break;

		case sizeof(uint32_t):
			return ptrswap4;
			break;

		case sizeof(uint64_t):
			return ptrswap8;
			break;

		default: // do nothing
			break;
	}
	// sometimes linters do not detect that all branches of switch have a return
	return ptrswap;
}

swapf chooseSwap(size_t membsize)
{
	switch (membsize) {
		case sizeof(uint8_t):
			return swap1;
			break;

		case sizeof(uint16_t):
			return swap2;
			break;

		case sizeof(uint32_t):
			return swap4;
			break;

		case sizeof(uint64_t):
			return swap8;
			break;

		default: // do nothing
			break;
	}
	// sometimes linters do not detect that all branches of switch have a return
	return swap;
}

void insert4sink(char *iptr, char *jptr, size_t membsize)
{
	char tmp[membsize]; // TODO: might cause stack-overflow?
	// store v in tmp
	memmove(tmp, iptr, membsize);
	// make room for new element j
	memmove(jptr + membsize, jptr, iptr - jptr);
	// copy v into j
	memmove(jptr, tmp, membsize);
}

#define insert4sinkX(T)                            \
	/* store v in tmp */                           \
	T tmp = *(T *)iptr;                            \
	/* make room for new element j */              \
	memmove(jptr + membsize, jptr, iptr - jptr);   \
	/* copy v into j */                            \
	*(T *)jptr = tmp;                              \


void insert4sink1(char *iptr, char *jptr, size_t membsize)
{
	insert4sinkX(uint8_t);
}

void insert4sink2(char *iptr, char *jptr, size_t membsize)
{
	insert4sinkX(uint16_t);
}

void insert4sink4(char *iptr, char *jptr, size_t membsize)
{
	insert4sinkX(uint32_t);
}

void insert4sink8(char *iptr, char *jptr, size_t membsize)
{
	insert4sinkX(uint64_t);
}

typedef void (*inserterf)(char *, char *, size_t);


// insertsort from right to left ("sink-sort")
void insertsort_sink(char *loptr, char *hiptr, size_t membsize, comparf cmpf, inserterf insert)
{
	char * const end = hiptr + membsize; // stop condition
	char *iptr = loptr + membsize;       // i pointer to next element to insert (v)
	// We start with [lo, lo] sorted array
	// [lo, i-1] items are sorted, we sink the ith element
	for (; iptr < end; iptr += membsize) {
		char *jptr = iptr; // rover pointer of the element to insert
		// move j to the left if the current left of j is greater than v
		while (loptr < jptr && cmpf(iptr, jptr - membsize) < 0) {
			jptr -= membsize;
		}
		// is it already in place?
		if (jptr != iptr) {
			insert(iptr, jptr, membsize);
		}
	}
}

void insert4ballon(char *iptr, char *jptr, size_t membsize)
{
	char tmp[membsize]; // TODO: might cause stack-overflow?
	// store v in tmp
	memmove(tmp, iptr, membsize);
	// make room for new element j
	memmove(iptr, iptr + membsize, jptr - iptr);
	// copy v into j
	memmove(jptr, tmp, membsize);
}

#define insert4ballonX(T)                          \
	/* store v in tmp */                           \
	T tmp = *(T *)iptr;                            \
	/* make room for new element j */              \
	memmove(iptr, iptr + membsize, jptr - iptr);   \
	/* copy v into j */                            \
	*(T *)jptr = tmp;                              \

void insert4ballon1(char *iptr, char *jptr, size_t membsize)
{
	insert4ballonX(uint8_t);
}

void insert4ballon2(char *iptr, char *jptr, size_t membsize)
{
	insert4ballonX(uint16_t);
}

void insert4ballon4(char *iptr, char *jptr, size_t membsize)
{
	insert4ballonX(uint32_t);
}

void insert4ballon8(char *iptr, char *jptr, size_t membsize)
{
	insert4ballonX(uint64_t);
}


// insertsort left to right ("balloon-sort")
void insertsort_ballon(char *loptr, char *hiptr, size_t membsize, comparf cmpf, inserterf insert)
{
	char * const end = loptr - membsize;  // stop condition
	char *iptr = hiptr - membsize;        // i pointer to next element to insert (v)
	// We start with [hi, hi] sorted array
	// [i-1, hi] items are sorted, we rise the ith element
	for (; end < iptr; iptr -= membsize) {
		char *jptr = iptr; // rover pointer of the element to insert
		// move j to the right if the current right of j is less than v
		while (jptr < hiptr && cmpf(jptr + membsize, iptr) < 0) {
			jptr += membsize;
		}
		// is it already in place?
		if (jptr != iptr) {
			insert(iptr, jptr, membsize);
		}
	}
}

// sort hi-lo+1 elements of membsize bytes in a[lo, hi] using cmpf
void insertSort(char *data, size_t lo, size_t hi, size_t membsize, comparf cmpf)
{
	char *loptr = &data[lo * membsize];
	char *hiptr = &data[hi * membsize];
	inserterf inserter;

#if 1
	switch (membsize) {
		case sizeof(uint8_t):
			inserter = insert4sink1;
			break;

		case sizeof(uint16_t):
			inserter = insert4sink2;
			break;

		case sizeof(uint32_t):
			inserter = insert4sink4;
			break;

		case sizeof(uint64_t):
			inserter = insert4sink8;
			break;

		default:
			inserter = insert4sink;
			break;
	}

	insertsort_sink(loptr, hiptr, membsize, cmpf, inserter);
#else
	switch (membsize) {
		case sizeof(uint8_t):
			inserter = insert4ballon1;
			break;

		case sizeof(uint16_t):
			inserter = insert4ballon2;
			break;

		case sizeof(uint32_t):
			inserter = insert4ballon4;
			break;

		case sizeof(uint64_t):
			inserter = insert4ballon8;
			break;

		default:
			inserter = insert4ballon;
			break;
	}

	insertsort_ballon(loptr, hiptr, membsize, cmpf, inserter);
#endif
}

// return the index of the median element among data[i], data[j], and data[k]
size_t median3(char *data, size_t i, size_t j, size_t k, size_t membsize, comparf cmpf, ptrswapf exch)
{
	char *iptr = &data[i * membsize],
	     *jptr = &data[j * membsize],
	     *kptr = &data[k * membsize];
	// ensure i <= j if j < i (it could be i == j)
	if (cmpf(jptr, iptr) < 0) {
		exch(jptr, iptr, membsize);
	}
	// check if j <= k already so that i <= j <= k
	if (cmpf(jptr, kptr) <= 0) {
		return j;
	}
	// ensure j < k when k < j (notice j != k)
	exch(jptr, kptr, membsize);
	// now we have j < k and i <= k (notice we just swapped j <-> k)
	// ensure i <= j when j < i (it could be i == j again)
	if (cmpf(jptr, iptr) < 0) {
		exch(jptr, iptr, membsize);
	}
	// i <= j < k
	return j;
}

size_t selectPivot(char *data, size_t n, size_t lo, size_t hi, size_t membsize, comparf cmpf, ptrswapf exch)
{
	// simple median of 3
	return median3(data, lo, lo + n / 2, hi, membsize, cmpf, exch);
}

void *slowSelect(char *data, size_t lo, size_t hi, size_t membsize, comparf cmpf, size_t k)
{
	insertSort(data, lo, hi, membsize, cmpf);

	if (k < lo) {
		return &data[lo * membsize];

	} else if (hi < k) {
		return &data[hi * membsize];
	}

	return &data[k * membsize];
}

