#!/bin/bash
#
# MIT License
#
# Copyright (c) 2021 Eduardo Miravalls Sierra
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

valgrind_benchmark_no_dups() {
	for i in $(seq 1 100); do
		valgrind --track-origins=yes ./benchmark.exe 100001 2>&1
		echo
	done &> valgoutput.txt
}

benchmark_no_dups() {
	for i in $(seq 1 100); do
		./benchmark.exe 10000001 2>&1
		echo
	done &> output.txt
}

valgrind_benchmark_dups() {
	for i in $(seq 1 100); do
		valgrind --track-origins=yes ./benchmark.exe 100001 t 0.5 2>&1
		echo
	done &> valgoutput2.txt
}

benchmark_w_dups() {
	for i in $(seq 1 100); do
		./benchmark.exe 10000001 t 0.5 2>&1
		echo
	done &> output2.txt
}


valgrind_benchmark_no_dups &
benchmark_no_dups &
valgrind_benchmark_dups &
benchmark_w_dups &

wait
