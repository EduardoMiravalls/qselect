/*
 * MIT License
 *
 * Copyright (c) 2021 Eduardo Miravalls Sierra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef _LIB_QSELECT3_H_
#define _LIB_QSELECT3_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include "auxqselect.h"

/**
 * @brief      quicksort 3-way partition function.
 * Puts all pivot element in place, so that all the elements less than the pivot
 * are swapped to its left, the rest are moved to its right.
 * The pivot is selected using median3().
 *
 * @param      data       pointer to first element in the array.
 * @param      n          number of elements in data.
 * @param      lo         first element index to check.
 * @param      hi         last element index to check.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 * @param      cmpf       pointer to comparison function.
 * @param      exch       pointer to ptrswap function.
 * @param      lt         used to store the index of the left-most pivot. Elements less than the pivot start at data[*lt - 1].
 * @param      gt         used to store the index of the right-most pivot. Elements greater than the pivot start at data[*gt + 1].
 *
 * @return     pointer to element of membsize bytes that goes at index k
 */
void threeWayPartition(char *data, size_t n, size_t lo, size_t hi, size_t membsize, comparf cmpf, ptrswapf exch, size_t *lt, size_t *gt);

/**
 * @brief      find the element that goes at index k using 3-way partition.
 *
 * @param      data       pointer to first element in the array.
 * @param      nmemb      number of elements of membsize bytes each in the array.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 * @param      cmpf       pointer to comparison function.
 * @param      k          valid element index in data.
 *
 * @return     pointer to element of membsize bytes that goes at index k
 */
void *qselect3way(void *data, size_t nmemb, size_t membsize, comparf cmpf, size_t k);

/**
 * @brief      sort the array using quicksort with the 3-way partition.
 * When the array contains <= CUTOFF_SIZE elements, it switches to insertsort.
 *
 * @param      data       pointer to first element in the array.
 * @param      nmemb      number of elements of membsize bytes each in the array.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 * @param      cmpf       pointer to comparison function.
 * @param      k          valid element index in data.
 *
 * @return     nothing (data is sorted in-place).
 */
void qsort3way(void *data, size_t nmemb, size_t membsize, comparf cmpf);

#ifdef  __cplusplus
}
#endif

#endif /* _LIB_QSELECT3_H_ */

