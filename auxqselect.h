/*
 * MIT License
 *
 * Copyright (c) 2021 Eduardo Miravalls Sierra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// Auxiliary functions to qselect and qselect3way

#ifndef _AUXQSELECT_H_
#define _AUXQSELECT_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include <stdlib.h>

// a global counter for the number of calls to cmpints
extern size_t ICMPS;
// a global counter for the number of calls to swap/ptrswap
extern size_t SWAPS;
// set the value of ICMPS and SWAPS to 0
void resetInstrumentation(void);

/* a typedef for a comparison function for qsort */
typedef int (*comparf)(const void *, const void *);

/**
 * @brief      compares two ints.
 *
 * @param      a     pointer to int.
 * @param      b     pointer to int.
 *
 * @return      0 if a == b
 * @return    < 0 if a < b
 * @return    > 0 if a > b
 */
int cmpints(const int *a, const int *b);

/**
 * @brief      compares two shorts.
 *
 * @param      a     pointer to short.
 * @param      b     pointer to short.
 *
 * @return      0 if a == b
 * @return    < 0 if a < b
 * @return    > 0 if a > b
 */
int cmpshorts(const short *a, const short *b);

/**
 * @brief      compares two chars.
 *
 * @param      a     pointer to chars.
 * @param      b     pointer to chars.
 *
 * @return      0 if a == b
 * @return    < 0 if a < b
 * @return    > 0 if a > b
 */
int cmpchars(const char *a, const char *b);

/**
 * @brief      swap membsize bytes at addresses a and b
 *
 * @param      a         pointer to valid element.
 * @param      b         pointer to valid element.
 * @param      membsize  number of bytes each element uses.
 *
 * @return     nothing
 */
void ptrswap(void *a, void *b, size_t membsize);

/* a typedef for the ptrswap() family */
typedef void (*ptrswapf)(void *, void *, size_t);

/* The following functions ptrswapX assume membsize has a value X. See ptrswap() */
void ptrswap1(void *a, void *b, size_t membsize);
void ptrswap2(void *a, void *b, size_t membsize);
void ptrswap4(void *a, void *b, size_t membsize);
void ptrswap8(void *a, void *b, size_t membsize);

/**
 * @brief      pick the appropriate ptrswap() function based on membsize.
 *
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 *
 * @return     ptrswapf
 */
ptrswapf choosePtrswap(size_t membsize);

/**
 * @brief      swap membsize bytes of elements i and j in data.
 *
 * @param      data       pointer to first element in the array.
 * @param      i          valid element index in data.
 * @param      j          valid element index in data.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 *
 * @return     nothing
 */
void swap(void *data, size_t i, size_t j, size_t membsize);

/* a typedef for the swap() family */
typedef void (*swapf)(void *, size_t, size_t, size_t);

/* The following functions swapX assume membsize has a value X. See swap() */
void swap1(void *data, size_t i, size_t j, size_t membsize);
void swap2(void *data, size_t i, size_t j, size_t membsize);
void swap4(void *data, size_t i, size_t j, size_t membsize);
void swap8(void *data, size_t i, size_t j, size_t membsize);

/**
 * @brief      pick the appropriate swap() function based on membsize.
 *
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 *
 * @return     swapf
 */
swapf chooseSwap(size_t membsize);

/**
 * @brief      sort hi-lo+1 elements of membsize bytes in a[lo, hi] using cmpf in O(N**2).
 * Assumes (0 <= lo <= hi) are valid elements within the array.
 *
 * @param      data       pointer to first element in the array.
 * @param      lo         first element index to sort.
 * @param      hi         last element index to sort.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 * @param      cmpf       pointer to comparison function.
 *
 * @return     nothing (data is sorted in-place).
 */
void insertSort(char *data, size_t lo, size_t hi, size_t membsize, comparf cmpf);

/**
 * @brief      sort values at position i,j,k so that data[i] <= data[j] <= data[k].
 * Assumes (0 <= lo <= hi) are valid elements within the array.
 *
 * @param      data       pointer to first element in the array.
 * @param      i          valid element index in data.
 * @param      j          valid element index in data.
 * @param      k          valid element index in data.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 * @param      cmpf       pointer to comparison function.
 * @param      exch       pointer to ptrswap function.
 *
 * @return     j
 */
size_t median3(char *data, size_t i, size_t j, size_t k, size_t membsize, comparf cmpf, ptrswapf exch);

/**
 * @brief      sort values at position i,j,k so that data[i] <= data[j] <= data[k].
 * Assumes (0 <= lo <= hi) are valid elements within the array.
 *
 * @param      data       pointer to first element in the array.
 * @param      i          valid element index in data.
 * @param      j          valid element index in data.
 * @param      k          valid element index in data.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 * @param      cmpf       pointer to comparison function.
 * @param      exch       pointer to ptrswap function.
 *
 * @return     j
 */
size_t selectPivot(char *data, size_t n, size_t lo, size_t hi, size_t membsize, comparf cmpf, ptrswapf exch);

/**
 * @brief      find the element that goes at index k by sorting the elements [lo, hi] using insertsort.
 *
 * @param      data       pointer to first element in the array.
 * @param      lo         first element index to sort.
 * @param      hi         last element index to sort.
 * @param      membsize   number of bytes each element uses. To go from a[i] to a[i+1], the pointer must increase membsize bytes.
 * @param      cmpf       pointer to comparison function.
 * @param      k          valid element index in data.
 *
 * @return     pointer to element of membsize bytes that goes at index k
 */
void *slowSelect(char *data, size_t lo, size_t hi, size_t membsize, comparf cmpf, size_t k);

/* determines when qsort recursion switches to insertion sort */
#ifndef CUTOFF_SIZE
	#define CUTOFF_SIZE 16
#endif

#ifdef  __cplusplus
}
#endif

#endif /* _AUXQSELECT_H_ */

