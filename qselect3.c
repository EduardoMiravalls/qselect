/*
 * MIT License
 *
 * Copyright (c) 2021 Eduardo Miravalls Sierra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <assert.h>

#include "qselect3.h"

/* J. Bentley and D. McIlroy 3-way partition scheme:
 * Divide the array in three parts: less, center and greater
 * less contains the elements less than pivot
 * center contains the elements equals to pivot
 * greater contains the elements greater than pivot
 * While running, it keeps the center part in the left and right sides of the array
 *
 * +-------------------------------------------------------------+
 * |  == pivot  |  < pivot  |     ?     |  > pivot  |  == pivot  |
 * +-------------------------------------------------------------+
 *             ^             ^         ^             ^
 *            *lt            i         j            *gt
 *
 * after it is done, it swaps the top part of less with the left center and
 * the low part of greater with right center, and adjusts the values of *lt and *gt
 *
 * +-------------------------------------------------------------+
 * |   less   |               center               |   greater   |
 * +-------------------------------------------------------------+
 *             ^                                  ^
 *            *lt                                *gt
 */
void threeWayPartition(char *data, size_t n, size_t lo, size_t hi, size_t membsize, comparf cmpf, ptrswapf exch, size_t *lt, size_t *gt)
{
#define PUT_PIVOT_IN_HI 0
#if PUT_PIVOT_IN_HI
	void *pivot;
	size_t i = lo-1,  // index of gte. NOTE: first for starts at i == lo
	       j = hi,    // index of lte. NOTE: first for starts at j == hi - 1 (skips pivot at hi)
	       k;
	char *iptr = &data[i * membsize],
	     *jptr = &data[j * membsize],
	     *kptr; // dummy/temporary variable
	size_t pivotIdx = selectPivot(data, n, lo, hi, membsize, cmpf, exch);
	*lt = lo-1; // *lt is the index of the right part of the left-center. NOTE: initial lt is outside array
	*gt = hi;   // *gt is the index of the left part of the right center. NOTE: initial pivot is in hi

	if (hi <= lo) {
		return;
	}

	// put pivot in hi
	pivot = &data[pivotIdx * membsize];
	kptr = &data[hi * membsize];
	exch(kptr, pivot, membsize);
	pivot = kptr;

	for (;;) {
		int icmp, jcmp;
		// move from left to find an element that is not less (gte)
		// test (i < hi) not needed as pivot is in hi
		for (iptr += membsize; (icmp = cmpf(iptr, pivot)) < 0; iptr += membsize) ;
		// move from right to find an element that is not greater (lte)
		// test (lo < j) is not needed as median() has sorted lo <= mid <= hi
		for (jptr -= membsize; (jcmp = cmpf(pivot, jptr)) < 0; jptr -= membsize) ;
		// stop if pointers have crossed, but check if both pointers are in an element equal to pivot
		if (iptr == jptr) {
			// skip the element equal to pivot
			iptr += membsize;
			jptr -= membsize;
			break;

		} else if (iptr >= jptr) {
			assert(jptr == iptr-membsize);
			break;
		}
		// exchange lte and gte
		exch(iptr, jptr, membsize);
		// if left element equal to pivot, exchange to left end
		if (0 == jcmp) { // note jcmp now has the comparison of i
			(*lt)++;
			kptr = &data[*lt * membsize];
			exch(kptr, iptr, membsize);
		}
		// if right element equal to pivot, exchange to right end
		if (0 == icmp) { // note icmp now has the comparison of j
			(*gt)--;
			kptr = &data[*gt * membsize];
			exch(kptr, jptr, membsize);
		}
	}
	// put the pivots in place
	// swap left equals to center before i
	if (*lt != lo-1) { // k <= *lt does not work if lo == 0 unless the comparison is signed
		k = lo;
		kptr = &data[k * membsize];

		for (; k <= *lt; k++, kptr += membsize, jptr -= membsize) {
			exch(kptr, jptr, membsize);
		}
	}
	// swap right equals to center after i
	// at least the first pivot in hi is moved to i
	k = hi;
	kptr = &data[k * membsize];

	for (; k >= *gt; k--, kptr -= membsize, iptr += membsize) {
		exch(kptr, iptr, membsize);
	}
	// update indexes of center
	// *lt - 1 is the index of greatest element < pivot.
	*lt = (jptr + membsize - data) / membsize;
	// *gt + 1 is the index of least element > pivot.
	*gt = (iptr - membsize - data) / membsize;
#else
	void *pivot;
	size_t i = lo,   // index of gte. NOTE: first for starts at i == lo + 1 (skips pivot)
	       j = hi,   // index of lte. NOTE: first for starts at j == hi - 1 (skips hi because lo <= mid <= hi)
	       k;
	// Assume lo <= mid <= hi because of median3()
	size_t pivotIdx = selectPivot(data, n, lo, hi, membsize, cmpf, exch);
	char *iptr = &data[i * membsize],
	     *jptr = &data[j * membsize],
	     *kptr;
	*lt = lo;     // *lt - 1 is the index of greatest element < pivot.
	*gt = hi+1;   // *gt + 1 is the index of least element > pivot. NOTE: initial gt is outside array

	if (hi <= lo) {
		return;
	}

	// put pivot in lo
	pivot = &data[pivotIdx * membsize];
	kptr = &data[lo * membsize];
	exch(kptr, pivot, membsize);
	pivot = kptr;

	for (;;) {
		int icmp, jcmp;
		// move from left to find an element that is not less (gte)
		// test (i < hi) not needed as pivot is in hi
		for (iptr += membsize; (icmp = cmpf(iptr, pivot)) < 0; iptr += membsize) ;
		// move from right to find an element that is not greater (lte)
		// test (lo < j) is not needed as median() has sorted lo <= mid <= hi
		for (jptr -= membsize; (jcmp = cmpf(pivot, jptr)) < 0; jptr -= membsize) ;
		// stop if pointers have crossed, but check if both pointers are in an element equal to pivot
		if (iptr == jptr) {
			// skip the element equal to pivot
			iptr += membsize;
			jptr -= membsize;
			break;

		} else if (iptr >= jptr) {
			assert(jptr == iptr-membsize);
			break;
		}
		// exchange lte and gte
		exch(iptr, jptr, membsize);
		// if left element equal to pivot, exchange to left end
		if (0 == jcmp) { // note jcmp now has the comparison of i
			(*lt)++;
			kptr = &data[*lt * membsize];
			exch(kptr, iptr, membsize);
		}
		// if right element equal to pivot, exchange to right end
		if (0 == icmp) { // note icmp now has the comparison of j
			(*gt)--;
			kptr = &data[*gt * membsize];
			exch(kptr, jptr, membsize);
		}
	}
	// put the pivots in place
	// swap left equals to center before i
	// at least the first pivot in lo is moved to j
	k = lo;
	kptr = &data[k * membsize];

	for (; k <= *lt; k++, kptr += membsize, jptr -= membsize) {
		exch(kptr, jptr, membsize);
	}
	// swap right equals to center after i
	k = hi;
	kptr = &data[k * membsize];

	for (; k >= *gt; k--, kptr -= membsize, iptr += membsize) {
		exch(kptr, iptr, membsize);
	}
	// update indexes of center
	// *lt - 1 is the index of greatest element < pivot. NOTE: *lt == lo is possible
	*lt = (jptr + membsize - data) / membsize;
	// *gt + 1 is the index of least element > pivot
	*gt = (iptr - membsize - data) / membsize;
#endif
}

void *_qselect3way(char *data, size_t lo, size_t hi, size_t membsize, comparf cmpf, ptrswapf exch, size_t k)
{
	const size_t n = hi - lo + 1;
	size_t lt = lo, gt = hi;

	if (hi <= lo) {
		return &data[membsize * lo];

	} else if (n <= CUTOFF_SIZE) {
		return slowSelect(data, lo, hi, membsize, cmpf, k);
	}

	assert(lo <= k && k <= hi);
	threeWayPartition(data, n, lo, hi, membsize, cmpf, exch, &lt, &gt);

	if (gt < k) {
		return _qselect3way(data, gt + 1, hi, membsize, cmpf, exch, k);

	} else if (k < lt) {
		return _qselect3way(data, lo, lt - 1, membsize, cmpf, exch, k);
	}

	return &data[k * membsize]; // lt <= k <= gt -> pivot is at k
}

void *qselect3way(void *data, size_t nmemb, size_t membsize, comparf cmpf, size_t k)
{
	ptrswapf exch = choosePtrswap(membsize);

	if (nmemb <= 1) {
		return data;
	}

	if (k < nmemb) {
		return _qselect3way(data, 0, nmemb - 1, membsize, cmpf, exch, k);

	} else {
		return _qselect3way(data, 0, nmemb - 1, membsize, cmpf, exch, nmemb - 1);
	}
}

void _qsort3way(char *data, size_t lo, size_t hi, size_t membsize, comparf cmpf, ptrswapf exch)
{
	const size_t n = hi - lo + 1;
	size_t lt = lo, gt = hi;

	if (hi <= lo) {
		return;

	} else if (n <= CUTOFF_SIZE) {
		insertSort(data, lo, hi, membsize, cmpf);
		return;
	}

	threeWayPartition(data, n, lo, hi, membsize, cmpf, exch, &lt, &gt);

	// recurse on the smallest array first
	if (lo == lt) {
		_qsort3way(data, gt + 1, hi, membsize, cmpf, exch);

	} else if (lt-lo <= hi-gt) {
		_qsort3way(data, lo, lt - 1, membsize, cmpf, exch);
		_qsort3way(data, gt + 1, hi, membsize, cmpf, exch);

	} else {
		_qsort3way(data, gt + 1, hi, membsize, cmpf, exch);
		_qsort3way(data, lo, lt - 1, membsize, cmpf, exch);
	}

}

void qsort3way(void *data, size_t nmemb, size_t membsize, comparf cmpf)
{
	ptrswapf exch = choosePtrswap(membsize);

	if (nmemb <= 1) {
		return;
	}

	_qsort3way(data, 0, nmemb-1, membsize, cmpf, exch);
}

