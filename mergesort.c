/*
 * MIT License
 *
 * Copyright (c) 2021 Eduardo Miravalls Sierra
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string.h> // memmove
#include <stdlib.h>
#include <assert.h>
#include <inttypes.h>

#include "auxqselect.h"

void *copy1(void *dst, const void *src, size_t membsize)
{
	(void)membsize;
	*(uint8_t *)dst = *(const uint8_t *)src;
	return dst;
}

void *copy2(void *dst, const void *src, size_t membsize)
{
	(void)membsize;
	*(uint16_t *)dst = *(const uint16_t *)src;
	return dst;
}

void *copy4(void *dst, const void *src, size_t membsize)
{
	(void)membsize;
	*(uint32_t *)dst = *(const uint32_t *)src;
	return dst;
}

void *copy8(void *dst, const void *src, size_t membsize)
{
	(void)membsize;
	*(uint64_t *)dst = *(const uint64_t *)src;
	return dst;
}

typedef void *(*copyf)(void *, const void *, size_t);

void merge(char *lo, char *midptr, char *hi, size_t membsize, comparf cmpf, copyf copy, char *tmp)
{
	/* as we copy [lo,mid-1] in tmp, we can't compare a pointer in tmp to midptr,
	 * so we use 2 pointers that we update at the same time so they act as an index */
	char *iptr = lo;     /* "read pointer" in [lo, mid-1] */
	char *jptr = midptr; /* read pointer in [mid, hi] */
	char *kptr = lo;     /* write pointer */
	/* check if already sorted */
	if ( hi <= lo || hi < midptr || cmpf(midptr - membsize, midptr) <= 0) {
		return;
	/* check if only 2 elements and, since we already tested it, unsorted! */
	} else if ((size_t)(hi - lo) == membsize) {
		/* swap them */
		copy(tmp, lo, membsize);
		copy(lo, hi, membsize);
		copy(hi, tmp, membsize);
		return;
	}
	/* copy [lo, mid-1] in tmp */
	memmove(tmp, lo, midptr - lo);
	/* check if blocks [lo, mid-1] and [mid, hi] are inverted */
	if (cmpf(hi, lo) < 0) {
		/* number of bytes in [mid,hi]. + membsize because hi is included */
		const size_t rightSize = hi + membsize - midptr;
		/* copy [mid, hi] in [lo, mid-1] */
		memmove(lo, midptr, rightSize);
		/* copy [lo,mid-1] in [mid, hi] if mid is even, or in [mid+1,hi] if mid is odd */
		memmove(lo + rightSize, tmp, midptr - lo);
		return;
	}
	/* merge both ranges into lo until one is exhausted */
	while (iptr < midptr && jptr <= hi) {
		if (cmpf(tmp, jptr) <= 0) {
			copy(kptr, tmp, membsize);
			iptr += membsize;
			tmp += membsize;

		} else {
			copy(kptr, jptr, membsize);
			jptr += membsize;
		}

		kptr += membsize;
	}
	/* copy the rest of tmp into the array */
	memmove(kptr, tmp, midptr - iptr);
	/* no need to copy the rest of jptr -> hi as they are already in place */
}

static inline
void merge1(char *lo, char *midptr, char *hi, size_t membsize, comparf cmpf, char *tmp)
{
	merge(lo, midptr, hi, membsize, cmpf, (copyf)copy1, tmp);
}

static inline
void merge2(char *lo, char *midptr, char *hi, size_t membsize, comparf cmpf, char *tmp)
{
	merge(lo, midptr, hi, membsize, cmpf, (copyf)copy2, tmp);
}

static inline
void merge4(char *lo, char *midptr, char *hi, size_t membsize, comparf cmpf, char *tmp)
{
	merge(lo, midptr, hi, membsize, cmpf, (copyf)copy4, tmp);
}

static inline
void merge8(char *lo, char *midptr, char *hi, size_t membsize, comparf cmpf, char *tmp)
{
	merge(lo, midptr, hi, membsize, cmpf, (copyf)copy8, tmp);
}

static inline
void mergeN(char *lo, char *midptr, char *hi, size_t membsize, comparf cmpf, char *tmp)
{
	merge(lo, midptr, hi, membsize, cmpf, (copyf)memmove, tmp);
}

/* macro implementation of a recursive merge sort with arbitrary msort and merge functions */
#define MSORT_IMPL(msort,merge)                               \
	const size_t mid = nmemb / 2;                             \
	char *midptr = lo + mid * membsize;                       \
	/* check if sorted or too short to sort with msort */     \
	if (nmemb <= CUTOFF_SIZE) {                               \
		insertSort(lo, 0, nmemb-1, membsize, cmpf);           \
		return;                                               \
	}                                                         \
	/* [lo, mid-1] */                                         \
	msort(lo, midptr - membsize, mid, membsize, cmpf, tmp);   \
	/* [mid, hi] */                                           \
	msort(midptr, hi, nmemb - mid, membsize, cmpf, tmp);      \
	merge(lo, midptr, hi, membsize, cmpf, tmp);               \


static
void _msort1(char *lo, char *hi, size_t nmemb, size_t membsize, comparf cmpf, void *tmp)
{
	MSORT_IMPL(_msort1, merge1);
}

static
void _msort2(char *lo, char *hi, size_t nmemb, size_t membsize, comparf cmpf, void *tmp)
{
	MSORT_IMPL(_msort2, merge2);
}

static
void _msort4(char *lo, char *hi, size_t nmemb, size_t membsize, comparf cmpf, void *tmp)
{
	MSORT_IMPL(_msort4, merge4);
}

static
void _msort8(char *lo, char *hi, size_t nmemb, size_t membsize, comparf cmpf, void *tmp)
{
	MSORT_IMPL(_msort8, merge8);
}

static
void _msortN(char *lo, char *hi, size_t nmemb, size_t membsize, comparf cmpf, void *tmp)
{
	MSORT_IMPL(_msortN, mergeN);
}

typedef void (*sorter)(char *, char *, size_t, size_t, comparf, void *);

sorter chooseSorter(size_t membsize)
{
	switch (membsize) {
		case sizeof(uint8_t):
			return _msort1;
			break;

		case sizeof(uint16_t):
			return _msort2;
			break;

		case sizeof(uint32_t):
			return _msort4;
			break;

		case sizeof(uint64_t):
			return _msort8;
			break;

		default: // do nothing
			break;
	}
	// sometimes linters do not detect that all branches of switch have a return
	return _msortN;
}

void mergeIndirect(void **ptrs, size_t lo, size_t mid, size_t hi, comparf cmpf, void **tmp)
{
	size_t i = lo,
	       j = mid,
	       k = lo;
	/* check if already sorted */
	if (hi <= lo || hi < mid || cmpf(ptrs[mid-1], ptrs[mid]) <= 0) {
		return;
	/* check if only 2 elements and, since we already tested it, unsorted! */
	} else if ((hi - lo) == 1) {
		/* swap them */
		tmp[0] = ptrs[lo];
		ptrs[lo] = ptrs[hi];
		ptrs[hi] = tmp[0];
		return;
	}
	/* copy [lo, mid-1] in tmp */
	memmove(tmp, ptrs + lo, (mid-lo)*sizeof(*ptrs));
	/* check if blocks [lo, mid-1] and [mid, hi] are inverted */
	if (cmpf(ptrs[hi], ptrs[lo]) < 0) {
		/* number of bytes in [mid,hi]. + 1 because hi is included */
		const size_t rightSize = hi + 1 - mid;
		/* copy [mid, hi] in [lo, mid-1] */
		memmove(ptrs + lo, ptrs + mid, rightSize*sizeof(*ptrs));
		/* copy [lo,mid-1] in [mid, hi] if mid is even, or in [mid+1,hi] if mid is odd */
		memmove(ptrs + lo + rightSize, tmp, (mid - lo)*sizeof(*ptrs));
		return;
	}
	/* merge both ranges into lo until one is exhausted */
	for (k = lo, i = lo; i < mid && j <= hi; k++) {
		if (cmpf(*tmp, ptrs[j]) <= 0) {
			ptrs[k] = *tmp;
			tmp++;
			i++;

		} else {
			ptrs[k] = ptrs[j];
			j++;
		}
	}
	/* copy the rest of tmp into the array */
	memmove(ptrs + k, tmp, (mid - i) * sizeof(*ptrs));
	/* no need to copy the rest of j -> hi as they are already in place */
}

static
void _msortIndirect(void **ptrs, size_t lo, size_t hi, size_t nmemb, comparf cmpf, void **tmp)
{
	const size_t m = nmemb / 2;
	const size_t mid = lo + m;
	/* check if sorted */
	if (nmemb <= 1) {
		//insertSort(lo, 0, nmemb-1, membsize, cmpf);
		return;
	}
	/* [lo, mid-1] */
	_msortIndirect(ptrs, lo, mid - 1, m, cmpf, tmp);
	/* [mid, hi] */
	_msortIndirect(ptrs, mid, hi, nmemb - m, cmpf, tmp);
	mergeIndirect(ptrs, lo, mid, hi, cmpf, tmp);
}

/* Related to Donald E. Knuth TAOCP vol 3 Section 5.2, when copies/swaps are expensive
 * it is better to sort using an address table (sort the pointers), then undo the permutation
 */
int addressTableSort(char *data, size_t nmemb, size_t membsize, comparf cmpf)
{
	size_t i, j;
	char *pi, *pj;   // rover pointers to data to save multiplications
	/* allocate enough memory to hold an extra element or the address table */
	const size_t ptrsSize = nmemb*(sizeof(void *)/2);
	const size_t tmpSize = (ptrsSize < membsize ? membsize : ptrsSize);
	/* allocate pointer array and tmp */
	void **ptrs = calloc(nmemb, sizeof(*ptrs)),
	     **tmp = calloc(1, tmpSize);

	if (NULL == ptrs || NULL == tmp) {
		free(ptrs);
		free(tmp);
		return -1;
	}
	/* initialise address table (pointers) */
	for (i = 0, pi = data; i < nmemb; i++, pi += membsize) {
		ptrs[i] = pi;
	}
	/* sort pointers */
	_msortIndirect(ptrs, 0, nmemb-1, nmemb, cmpf, tmp);
	/* shuffle data using the address table.
	 * Based on https://www.techiedelight.com/shuffle-array-according-to-given-order/
	 * but the algorithm is related to TAOCP vol 3 Section 5.2, exercise 10.
	 */
	for (i = 0, pi = data; i < nmemb; i++, pi += membsize) {
		if (ptrs[i] == pi) {
			continue; /* ith element is in place */
		}
		/* optimize swaps */
		memmove(tmp, ptrs[i], membsize);
		/* ptrs[i] says the ith element is at an address that should be stored in ptrs[j],
		 * swap the elements and pointers i and j, now i contains the pointer that
		 * was in ptrs[j], and ptrs[j] is now in place
		 * Repeat the swaps until the ith element is in place, that is:
			while (ptrs[i] != pi) {
				j = ((size_t)((char *)ptrs[i] - data)) / membsize;
				ptrswap(ptrs[i], ptrs[j], membsize);
				swap(ptrs, i, j, sizeof(void *));
			}

		 * which is the same as

			while (ptrs[i] != pi) {
				j = ((size_t)((char *)ptrs[i] - data)) / membsize;
				copy(tmp, ptrs[i], membsize);
				copy(ptrs[i], ptrs[j] membsize);
				copy(ptrs[j], tmp, membsize);
				swap(ptrs, i, j, sizeof(void *));
			}

		 * However, since we are always looking for the same element i, we do not need to keep swapping
		 * the expensive elements if we store the initial value at i in a temporal value (tmp)
		 * and only update the values at ptrs[i]. After we swapped the pointers i and j, we can continue
		 * the cycle until we find the position were tmp goes, and finally store tmp at ptrs[i]
			copy(tmp, ptrs[i], membsize)

			while (ptrs[i] != pi) {
				copy(ptrs[i], ptrs[j], membsize);
				swap(ptrs, i, j, sizeof(void *));
			}

			copy(ptrs[i], tmp, membsize)
		 */
		while (ptrs[i] != pi) {
			/* find index of the element to swap with */
			j = ((size_t)((char *)ptrs[i] - data)) / membsize;
			/* update data at ptrs[i], which is now in place */
			memmove(ptrs[i], ptrs[j], membsize);
			/* swap pointers */
			pj = ptrs[j];
			ptrs[j] = ptrs[i];
			ptrs[i] = pj;
		}
		/* the initial value goes at the end of the permutation */
		memmove(ptrs[i], tmp, membsize);
	}

	free(ptrs);
	free(tmp);
	return 0;
}

void msort(void *data, size_t nmemb, size_t membsize, comparf cmpf)
{
#ifndef MSORT_STACK_ALLOC
	#define MSORT_STACK_ALLOC 1024
#endif
	void *tmp = NULL;
	char *lo = data;
	char *hi = lo + (nmemb - 1) * membsize;
	sorter sort = chooseSorter(membsize);

	if (nmemb <= 1) {
		return;

	} else if (nmemb*membsize <= MSORT_STACK_ALLOC) {
		char stacktmp[MSORT_STACK_ALLOC];
		sort(lo, hi, nmemb, membsize, cmpf, stacktmp);
		return;

	} else if (sizeof(uint64_t) < membsize) {
		addressTableSort(data, nmemb, membsize, cmpf);
		return;
	}

	tmp = calloc(nmemb / 2, membsize);

	if (NULL == tmp) {
		return;
	}

	sort(lo, hi, nmemb, membsize, cmpf, tmp);
	free(tmp);
}
