# MIT License
#
# Copyright (c) 2021 Eduardo Miravalls Sierra
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

.PHONY: debug clean all

SOURCES = qselect.c qselect3.c auxqselect.c mergesort.c
HEADERS = qselect.h qselect3.h auxqselect.h
CFLAGS = -Wall -Wextra -DMAX_CHUNK_SIZE=16
CC = gcc

all:	libqselect.a libqselect3.a


libqselect.a:	CFLAGS += -O3 -DNDEBUG
libqselect.a:	$(SOURCES) $(HEADERS)
	$(CC) $(CFLAGS) -c $(SOURCES)
	ar -rc $@ qselect.o auxqselect.o
	ranlib -D $@

libqselect3.a:	CFLAGS += -O3 -DNDEBUG
libqselect3.a:	$(SOURCES) $(HEADERS)
	$(CC) $(CFLAGS) -c $(SOURCES)
	ar -rc $@ qselect3.o auxqselect.o
	ranlib -D $@

benchmark.exe:	CFLAGS += -O3 -DNDEBUG -g
benchmark.exe:	libqselect.a libqselect3.a benchmark.c
	$(CC) $(CFLAGS) -DINSTRUMENT=1 $(SOURCES) benchmark.c -o $@
	./$@
	./$@ 100 t 0.5

debug:	CFLAGS += -O0 -g -DDEBUG
debug:	$(SOURCES) benchmark.c $(HEADERS)
	$(CC) $(CFLAGS) -DINSTRUMENT=1 $(SOURCES) benchmark.c -o benchmark.exe
	valgrind --track-origins=yes ./benchmark.exe 10001
	valgrind --track-origins=yes ./benchmark.exe 10001 t 0.5

clean:
	rm -f benchmark.exe *.o *.a a.out
